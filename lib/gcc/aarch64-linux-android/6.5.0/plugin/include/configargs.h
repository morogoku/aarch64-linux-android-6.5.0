/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --disable-multilib --disable-werror --enable-fix-cortex-a53-835769 --enable-fix-cortex-a53-843419 --enable-languages=c,c++ --target=aarch64-linux-android --prefix=/home/moro/kernel/build-tools-gcc-moro/aarch64-linux-android";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { NULL, NULL} };
